package com.example.kotlin.thi

import java.util.*

class StackLinkedList<T> {
    var list = LinkedList<T>()


    fun pop(): T? {
        return if(getSize()>0) {
            list.pop()
        } else {
            null
        }
    }

    fun peek(): T? {
        return list.peek()
    }

    fun isEmpty(): Boolean {
        return getSize() == 0
    }

    fun getSize(): Int {
        return list.size
    }

    fun put(input:T) {
        list.push(input)
    }

}

fun main() {
    val stack = StackLinkedList<Int>()
    println("-----------------------")
    println(stack.getSize())
    println(stack.peek())
    println(stack.pop())
    println(stack.isEmpty())
    println("-----------------------")
    stack.put(1)
    stack.put(2)
    println(stack.getSize())
    println(stack.peek())
    println(stack.pop())
    println(stack.pop())
}