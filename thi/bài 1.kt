package com.example.kotlin.thi

fun palindrome(input:String):Boolean {
    val trimed = input.trim()
    for(i in 0..trimed.length/2) {
        if (trimed[i] != trimed[trimed.length - i -1]) {
            return false
        }
    }
    return true
}

fun main() {
    println(palindrome("abba"))
    println(palindrome("abcdefg"))
    println(palindrome("abba "))
    println(palindrome("abb a"))
}