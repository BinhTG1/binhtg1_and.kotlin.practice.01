package com.example.kotlin.thi

import java.util.*

class Queue2Stacks<T> {
    var stack1 = Stack<T>()
    var stack2 = StackLinkedList<T>()


    fun dequeue(): T? {
        if(stack2.isEmpty()) {
            if(stack1.isEmpty()) {
                return null
            } else {
                while (!stack1.isEmpty()) {
                    stack1.pop()?.let { stack2.put(it) }
                }
            }
        }
        return stack2.pop()
    }

    fun peek(): T? {
        return stack2.peek()
    }

    fun isEmpty(): Boolean {
        return getSize() == 0
    }

    fun getSize(): Int {
        return stack1.getSize() + stack2.getSize()
    }

    fun enqueue(input:T) {
        stack1.put(input)
    }

}

fun main() {
    val queue = Queue2Stacks<Int>()
    println("-----------------------")
    queue.enqueue(1)
    println(queue.dequeue())
    println(queue.dequeue())
    println("-----------------------")
    val queueChar = Queue2Stacks<Char>()
    queueChar.isEmpty()
    queueChar.enqueue('A')
    println(queueChar.isEmpty())
    queueChar.enqueue('B')
    queueChar.enqueue('C')
    println(queueChar.dequeue())
    println(queueChar.peek())
    println(queueChar.peek())
    println(queueChar.dequeue())
    println(queueChar.dequeue())
    println(queueChar.dequeue())
}