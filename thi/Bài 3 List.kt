package com.example.kotlin.thi

class Queue<T> {
    var list : List<T> = ArrayList()

    fun dequeue(): T? {
        return if(getSize() > 0) {
            val a = list[0]
            list = list.drop(1)
            a
        } else {
            null
        }
    }

    fun peek(): T? {
        return if(getSize()>0){
            list[0]
        } else {
            null
        }
    }

    fun isEmpty(): Boolean {
        return getSize() == 0
    }

    fun getSize(): Int {
        return list.size
    }

    fun enqueue(input:T) {
        list = list.plus(listOf(input))
    }
}

fun main() {
    val queue = Queue<Int>()
    println("-----------------------")
    queue.enqueue(1)
    println(queue.dequeue())
    println(queue.dequeue())
    println("-----------------------")
    val queueChar = Queue<Char>()
    queueChar.isEmpty()
    queueChar.enqueue('A')
    println(queueChar.isEmpty())
    queueChar.enqueue('B')
    queueChar.enqueue('C')
    println(queueChar.dequeue())
    println(queueChar.peek())
    println(queueChar.peek())
    println(queueChar.dequeue())
    println(queueChar.dequeue())
    println(queueChar.dequeue())
}